mod net;

use std::error::Error;
//use std::thread;

use crossbeam::channel::Receiver;
use mio::net::TcpListener;
use mio::{Events, Interest, Poll, Token};
//use num_cpus::{get, get_physical};
use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;

fn main() -> Result<(), Box<dyn Error>> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    // Create Poll Instance.
    let mut poll = Poll::new()?;

    // Create Storage for events.
    let mut events = Events::with_capacity(128);

    // Setup the server socket.
    Ok(())
}
