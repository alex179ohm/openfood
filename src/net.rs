mod client;
mod server;

pub use client::Client;
pub use server::Server;

const LISTENER: mio::Token = mio::Token(0);
const CLIENT: mio::Token = mio::Token(1);
