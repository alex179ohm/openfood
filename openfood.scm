(use-modules (guix packages)
             (guix download)
             (guix build-system cargo)
             (guix licenses))

(package
 (name "openfood")
 (version "0.1")
 (build-system cargo-build-system)
 (synopsis "OpenFood makes foods decentralized")
 (description "OpenFood is a tool wich allows humans to creates food farms decentralized")
 (home-page "https://gitlab.com/alex179ohm/openfood")
 (license mit))
